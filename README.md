# GitBook COSS (compliant edition)

## Fork

The upstream repository is pretty much dead, that's why I didn't bother to create a mirror: https://github.com/unprotocols/gitbook-plugin-coss

This fork as two goals:
1. Comply to the URL syntax defined in the [RFC 1738](https://www.ietf.org/rfc/rfc1738.txt).
2. Allow development on Windows system with it's [restrictive file system characters](https://msdn.microsoft.com/en-us/library/windows/desktop/aa365247(v=vs.85).aspx).

## Functionality

This plugin is used to generate [COSS-compatible protocols](https://rfc.harbec.site/spec-1/).

It will prepend specification title to every specification and produce links
in the following forms:

* /spec-NN
* /spec-NN/NAME
* /NN (shortcut form)
* /NAME

It will also use YAML front matter to populate specification metainformation, unless `noinsert` is specified in the front matter.
